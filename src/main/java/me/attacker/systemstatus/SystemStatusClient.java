package me.attacker.systemstatus;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.concurrent.TimeUnit;

public class SystemStatusClient {

    private final ManagedChannel channel;
    private final int port;
    private final SystemStatusServiceGrpc.SystemStatusServiceBlockingStub blockingStub;


    public SystemStatusClient(int port) {
        this.port = port;
        channel = ManagedChannelBuilder.forAddress("localhost", port).usePlaintext(true).build();
        blockingStub = SystemStatusServiceGrpc.newBlockingStub(this.channel);

    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public void getSystemStatus() {
        SystemStatusRpc.SystemStatusRequest request = SystemStatusRpc.SystemStatusRequest.newBuilder().setData(4).build();
        SystemStatusRpc.SystemStatusResponse response = blockingStub.systemStatus(request);
        System.out.println(response.getAvailableMemory());
        System.out.println(response.getPlatform());
}

    public static void main(String[] args) {
        SystemStatusClient client = new SystemStatusClient(8980);
        client.getSystemStatus();
    }
}
