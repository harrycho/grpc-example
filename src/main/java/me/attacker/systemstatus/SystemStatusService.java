package me.attacker.systemstatus;

import io.grpc.stub.StreamObserver;
import me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse;

public class SystemStatusService extends SystemStatusServiceGrpc.SystemStatusServiceImplBase {


    @Override
    public void systemStatus(SystemStatusRpc.SystemStatusRequest request,
                             StreamObserver<SystemStatusResponse> responseObserver) {
        SystemStatusResponse response = SystemStatusResponse.newBuilder()
                .setAvailableMemory(60)
                .setPlatform(SystemStatusResponse.Platform.LINUX).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }


}
