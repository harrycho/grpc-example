package me.attacker.systemstatus;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class SystemStatusServer {

    private int port;
    private Server server;

    public SystemStatusServer(int port) {
        this.port = port;
        this.server = ServerBuilder.forPort(port).addService(new SystemStatusService()).build();
    }

    public void start() throws IOException {
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may has been reset by its JVM shutdown hook.
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            SystemStatusServer.this.stop();
            System.err.println("*** server shut down");
        }));
    }

    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws Exception {
        SystemStatusServer server = new SystemStatusServer(8980);
        server.start();
        server.blockUntilShutdown();
    }
}
