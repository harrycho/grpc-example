package me.attacker.systemstatus;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.10.1)",
    comments = "Source: system-status-rpc.proto")
public final class SystemStatusServiceGrpc {

  private SystemStatusServiceGrpc() {}

  public static final String SERVICE_NAME = "me.attacker.systemstatus.SystemStatusService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getSystemStatusMethod()} instead. 
  public static final io.grpc.MethodDescriptor<me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest,
      me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> METHOD_SYSTEM_STATUS = getSystemStatusMethodHelper();

  private static volatile io.grpc.MethodDescriptor<me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest,
      me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> getSystemStatusMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest,
      me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> getSystemStatusMethod() {
    return getSystemStatusMethodHelper();
  }

  private static io.grpc.MethodDescriptor<me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest,
      me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> getSystemStatusMethodHelper() {
    io.grpc.MethodDescriptor<me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest, me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> getSystemStatusMethod;
    if ((getSystemStatusMethod = SystemStatusServiceGrpc.getSystemStatusMethod) == null) {
      synchronized (SystemStatusServiceGrpc.class) {
        if ((getSystemStatusMethod = SystemStatusServiceGrpc.getSystemStatusMethod) == null) {
          SystemStatusServiceGrpc.getSystemStatusMethod = getSystemStatusMethod = 
              io.grpc.MethodDescriptor.<me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest, me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "me.attacker.systemstatus.SystemStatusService", "SystemStatus"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new SystemStatusServiceMethodDescriptorSupplier("SystemStatus"))
                  .build();
          }
        }
     }
     return getSystemStatusMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SystemStatusServiceStub newStub(io.grpc.Channel channel) {
    return new SystemStatusServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SystemStatusServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SystemStatusServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SystemStatusServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SystemStatusServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class SystemStatusServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void systemStatus(me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest request,
        io.grpc.stub.StreamObserver<me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSystemStatusMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSystemStatusMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest,
                me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse>(
                  this, METHODID_SYSTEM_STATUS)))
          .build();
    }
  }

  /**
   */
  public static final class SystemStatusServiceStub extends io.grpc.stub.AbstractStub<SystemStatusServiceStub> {
    private SystemStatusServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SystemStatusServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SystemStatusServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SystemStatusServiceStub(channel, callOptions);
    }

    /**
     */
    public void systemStatus(me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest request,
        io.grpc.stub.StreamObserver<me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSystemStatusMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SystemStatusServiceBlockingStub extends io.grpc.stub.AbstractStub<SystemStatusServiceBlockingStub> {
    private SystemStatusServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SystemStatusServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SystemStatusServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SystemStatusServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse systemStatus(me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest request) {
      return blockingUnaryCall(
          getChannel(), getSystemStatusMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SystemStatusServiceFutureStub extends io.grpc.stub.AbstractStub<SystemStatusServiceFutureStub> {
    private SystemStatusServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SystemStatusServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SystemStatusServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SystemStatusServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse> systemStatus(
        me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSystemStatusMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SYSTEM_STATUS = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SystemStatusServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SystemStatusServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SYSTEM_STATUS:
          serviceImpl.systemStatus((me.attacker.systemstatus.SystemStatusRpc.SystemStatusRequest) request,
              (io.grpc.stub.StreamObserver<me.attacker.systemstatus.SystemStatusRpc.SystemStatusResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SystemStatusServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SystemStatusServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return me.attacker.systemstatus.SystemStatusRpc.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SystemStatusService");
    }
  }

  private static final class SystemStatusServiceFileDescriptorSupplier
      extends SystemStatusServiceBaseDescriptorSupplier {
    SystemStatusServiceFileDescriptorSupplier() {}
  }

  private static final class SystemStatusServiceMethodDescriptorSupplier
      extends SystemStatusServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SystemStatusServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SystemStatusServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SystemStatusServiceFileDescriptorSupplier())
              .addMethod(getSystemStatusMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
